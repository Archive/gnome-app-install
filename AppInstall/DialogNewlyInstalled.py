# (c) 2005 Canonical, GPL

import pygtk
pygtk.require('2.0')
from SimpleGladeApp import SimpleGladeApp

import gtk
import gobject
import os

from Util import *

class DialogNewlyInstalled(SimpleGladeApp):
    def __init__(self, datadir, parent, to_add, cache):
        SimpleGladeApp.__init__(self,
                                path=datadir+"/gnome-app-install.glade",
                                root="dialog_newly_installed",
                                domain="gnome-app-install")
                                
        self.store = gtk.ListStore(gobject.TYPE_STRING,
                                   gobject.TYPE_PYOBJECT)
        for elm in to_add:
            if cache.has_key(elm.pkgname) and cache[elm.pkgname].isInstalled:
                self.store.append([elm.name,elm])                    
        self.treeview_newly_installed.set_model(self.store)
        self.dialog_newly_installed.set_transient_for(parent)
        self.init_treeview(self.treeview_newly_installed)

    def run(self):
        return self.dialog_newly_installed.run()

    def init_treeview(self, view):
        def package_view_func(cell_layout, renderer, model, iter):
            (name, item) = model.get(iter, 0, 1)
            menupath = "</b> &gt; <b>".join(item.menupath)
            menupath = "<b>" + menupath + "</b>"
            renderer.set_property("markup",
                                  "%s\n<small>Menu: %s</small>" % \
                                  (item.name, menupath))
        def icon_cell_func(column, cell, model, iter):
            menuitem = model.get_value(iter, 1)
            if menuitem.icon != None:
                cell.set_property("pixbuf", menuitem.icon)
            else:
                cell.set_property("pixbuf", None)
            cell.set_property("visible", True)
        view.set_search_column(COL_NAME)
        view.get_selection().set_mode(gtk.SELECTION_NONE)
        column = gtk.TreeViewColumn("")
        render = gtk.CellRendererPixbuf()
        column.pack_start(render, False)
        column.set_cell_data_func (render, icon_cell_func)
        render = gtk.CellRendererText()
        render.set_property("xpad", 4)
        column.pack_start(render, True)
        column.add_attribute(render, "markup", COL_NAME)
        column.set_cell_data_func (render, package_view_func)
        view.append_column(column)


    def on_treeview_newly_installed_row_activated(self, treeview_packages,
                                                  path, view_column):
        if os.getuid() == 0:
            return False
        treeiter = self.store.get_iter(path)
        (name, item) = self.store[treeiter]
        cmd_parts = []
        command = item.execCmd
        terminal = item.needsTerminal
        if command == "": return
        for part in command.split():
            while True:
                # two consecutive '%' characters represent an actual '%'
                if len(part) >= 2 and part[:2] == '%%':
                    cmd_parts.append('%')
                    part = part[2:]
                    continue
                # we're running the command without any options, so strip out placeholders
                if part[0] == '%': break
                # if the last part was an actual '%', we don't want to join it with a space, so do it by hand
                if cmd_parts[-1:] == '%':
                    part = '%' + part
                    cmd_parts[-1:] = part
                    break
                cmd_parts.append(part)
                break
        
        if terminal:
            command = " ".join(cmd_parts)
            command = "gnome-terminal --command=\"" + command + "\""
            cmd_parts = command.split()
        
        # run program
        os.spawnvp(os.P_NOWAIT, cmd_parts[0], cmd_parts)

    def hide(self):
        self.dialog_newly_installed.hide()
        

if __name__ == "__main__":
    from Menu import ApplicationMenu
    cache = apt.Cache()
    datadir = "/usr/share/gnome-app-install/"
    progress = apt.progress.OpTextProgress()
    to_add = []
    
    treeview_categories = gtk.TreeView()
    treeview_packages = gtk.TreeView()

    menu = ApplicationMenu(datadir, cache,
                           treeview_categories,
                           treeview_packages,
                           progress)
    

    to_add = set()
    for (type, name, item) in menu.store:
        for (type,name,item) in item.real_applications_store:
            to_add.add(item)
    print to_add
    dia = DialogNewlyInstalled(datadir, None,
                               to_add,
                               cache)
    dia.run()
    dia.hide()
