# (c) 2005 Canonical, GPL

from SimpleGladeApp import SimpleGladeApp
import gtk
import os
from gettext import gettext as _
from BrowserView import GtkHtml2BrowserView as BrowserView

class DialogUnavailable(SimpleGladeApp):

    def __init__(self, datadir, parent, item):
        SimpleGladeApp.__init__(self,
                                path=datadir+"/gnome-app-install.glade",
                                root="dialog_unavailable",
                                domain="gnome-app-install")
        if item.component:
            header = _("Add the required section \'%s\' of the Ubuntu software "\
                       "channel?" % item.component)
            msg = _("The section of the Ubuntu software channel "\
                    "that includes \'%s\' is not enabled.\nYou need a " \
                    "working internet connection to continue." % item.name)
        elif item.channel:
            header = _("Add the required software channel \'%s\'?" % \
                       item.component)
            msg = _("The software channel "\
                    "that includes \'%s\' is not enabled.\nYou need a " \
                    "working internet connection to continue." % item.name)
        self.dialog_unavailable.set_transient_for(parent)
        self.dialog_unavailable.realize()
        self.dialog_unavailable.window.set_functions(gtk.gdk.FUNC_MOVE)
        self.unavailable_label.set_markup("<b><big>%s</big></b>\n\n%s" %
                                          (header, msg))

    def run(self):
        return self.dialog_unavailable.run()

    def hide(self):
        self.dialog_unavailable.hide()


