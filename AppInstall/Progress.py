# (c) 2005 Canonical - GPL
#

import apt
import apt_pkg
import gtk

class GtkOpProgressWindow(apt.progress.OpProgress):
    def __init__(self, glade, parent):
        self.window_progress = glade.get_widget("window_progress")
        self.progressbar_cache = glade.get_widget("progressbar_cache")
        self.label_action = glade.get_widget("label_action")
        self.window_progress.realize()
        self.window_progress.window.set_functions(gtk.gdk.FUNC_MOVE)
        self.window_progress.set_transient_for(parent)
        self.progressbar_cache.set_pulse_step(0.005)
    def update(self, percent):
        #if not self.progress_window.visible():
        self.window_progress.show()
        #self.progressbar_cache.set_fraction(percent/100.0)
        if percent > 99:
            self.progressbar_cache.set_fraction(1)
        else:
            self.progressbar_cache.pulse()
        self.label_action.set_markup("<i>"+"%s" % self.subOp+"</i>")
        while gtk.events_pending():
            gtk.main_iteration()
    # using __del__ here sucks (because of eventual GC lazines)
    # but there is no "complete" callback in apt yet and "Done"
    # is called for each subOp
    def __del__(self):
        self.window_progress.hide()
