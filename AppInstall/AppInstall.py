# Copyright (C) 2004-2005 Ross Burton <ross@burtonini.com>
#               2005 Canonical
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 2 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA


import pygtk; pygtk.require("2.0")
import gtk
import gtk.glade
import gtk.gdk
import gobject
import gconf

import LaunchpadIntegration
from gettext import gettext as _

import time
import re
import subprocess
import tempfile
import warnings
import os
import sys
import shutil
from datetime import datetime

from warnings import warn
warnings.filterwarnings("ignore", "ICON:.*", UserWarning)
warnings.filterwarnings("ignore", "apt API not stable yet", FutureWarning)
import apt
import apt_pkg

# from update-manager, needs to be factored out
from SoftwareProperties import SoftwareProperties
from SoftwareProperties.aptsources import SourcesList

# internal imports
from DialogNewlyInstalled import DialogNewlyInstalled
from DialogPendingChanges import DialogPendingChanges
from DialogMultipleApps import DialogMultipleApps
from DialogUnavailable import DialogUnavailable
from DialogProprietary import DialogProprietary
from PackageWorker import PackageWorker
from Menu import MenuItem, Application, ApplicationMenu

# we support both moz and gtkhtml2 right now
from BrowserView import GtkHtml2BrowserView as BrowserView
#from BrowserView import MozEmbedBrowserView as BrowserView
#from BrowserView import DumpBrowserView as BrowserView
from SimpleGladeApp import SimpleGladeApp
from Progress import GtkOpProgressWindow
from Util import *

# this const must DIE DIE DIE
DISTRO_VERSION = "dapper"

# this is used to guess the desktop environment that the application
# was written for
desktop_environment_mapping = {
    ("kdelibs4c2a","python-kde3") :
    _("This program is designed for the KDE Desktop Environment"),
    ("libgnome2-0","python-gnome2") :
    _("This program is designed for the GNOME Desktop Environment"),
    ("libgnustep-base1.11",):
    _("This program is designed for the Gnustep Desktop Environment"),
    ("libxfce4util-1",) :
    _("This program is designed for the XFCE Desktop Environment"),
}
    

class MyCache(apt.Cache):
    def pkgDependsOn(self, pkgname, depends_name):
        """ check if a given pkg depends on a given dependencie """
        if not self.has_key(pkgname):
            return False
        pkg = self[pkgname]
        candver = self._depcache.GetCandidateVer(pkg._pkg)
	if candver == None:
		return False
	dependslist = candver.DependsList
	for dep in dependslist.keys():
            if dep == "Depends" or dep == "PreDepends":
                # get the list of each dependency object
                for depVerList in dependslist[dep]:
                    for z in depVerList:
                        # get all TargetVersions of
                        # the dependency object
                        for tpkg in z.AllTargets():
                            if depends_name == tpkg.ParentPkg.Name:
                                return True
        return False
        
        
class AppInstall(SimpleGladeApp):

    def __init__(self, datadir, desktopdir, arguments=None):
        SimpleGladeApp.__init__(self, domain="gnome-app-install",
                                path=datadir+"/gnome-app-install.glade")

        self.channelsdir = desktopdir+"/channels"       
        self.datadir = datadir
        self.desktopdir = desktopdir
        # this holds the filenames of the generated html pages
        self.info_pages = {}

        # setup a default icon
        icons = gtk.icon_theme_get_default()
        gtk.window_set_default_icon(icons.load_icon("gnome-settings-default-applications", 32, 0))
        #self.icons.prepend_search_path(os.path.join(self.menudir, "icons"))
        
        # FIXME: LaunchpadIntegration support, needs better sepearation
        #LaunchpadIntegration.set_sourcepackagename("gnome-app-install")
        #LaunchpadIntegration.add_items (self.menuitem_help_menu, -1, False, True);

        # sensitive stuff
        self.button_apply.set_sensitive(False)

        # Connect search widgets
        self.left_side = self.left_vbox
        #self.button_clear.set_sensitive(False)
                
        # Create the treeview store
        #base_column_types = (gobject.TYPE_INT, gobject.TYPE_BOOLEAN, gobject.TYPE_BOOLEAN, gobject.TYPE_STRING, gobject.TYPE_STRING, gtk.gdk.Pixbuf.__gtype__, gobject.TYPE_STRING, gobject.TYPE_STRING, object, gobject.TYPE_STRING, gobject.TYPE_BOOLEAN, gobject.TYPE_BOOLEAN, gobject.TYPE_STRING, gobject.TYPE_STRING)
        #self.store = gtk.TreeStore(*base_column_types)
        #self.multiple_store = gtk.ListStore(*base_column_types)
        #self.add_store = gtk.ListStore(*base_column_types)
        #self.remove_store = gtk.ListStore(*base_column_types)
        
        # Create the search results store
        #self.search_store = gtk.ListStore(*(base_column_types + (object,)))

        # create the treeview
        self.setupTreeview()

        # generate page
        self.intro_page = self.generateIntro()
        
        # Setup the browser
        # FIXME: make this a glade custom widget function
        #self.browser = BrowserView()
        #self.isInfoPage = True
        #self.browser_box.pack_start(self.browser)
        #self.browser.show()

        # now show the main window ...
        self.window_main.show()

        # setup the gconf backend
        self.config = gconf.client_get_default()
        self.config.add_dir ("/apps/gnome-app-install", gconf.CLIENT_PRELOAD_NONE)

        # ... and open the cache
        self.updateCache()

        # this is a set() of packagenames that contain multiple applications
        # if a pkgname is in the set, a dialog was already displayed to the
        # user about this (ugly ...)
        self.multiple_pkgs_seen = set()
        
        #self.isInfoPage = False

        # handle arguments
        if arguments is not None and len(arguments) > 1:
            for arg in arguments[1:]:
                if arg.startswith("--mime-type="):
                    self.search_entry.set_text("mime-type:%s" % arg[len("--mime-type="):])
                    self.searchClicked(None)
        # create a worker that does the actual installing etc
        self.packageWorker = PackageWorker()
        self.search_timeout_id = 0
      
    def create_custom_browser_view(self, s1, s2, i1, i2):
        self.browser = BrowserView()
        self.browser.connect("submit", self.on_browser_submit)
        self.browser.show()
        return self.browser

    def on_checkbutton_show_proprietary_toggled(self, button):
        #print "on_checkbutton_show_proprietary_toggled()"
        value = button.get_active()
        self.menu.show_proprietary = value
        self.config.set_bool("/apps/gnome-app-install/show_proprietary", value)

    def on_checkbutton_show_unsupported_toggled(self, button):
        #print "on_checkbutton_show_unsupported_toggled()"
        value = button.get_active()
        self.menu.show_unsupported = value
        self.config.set_bool("/apps/gnome-app-install/show_unsupported", value)

    # install toggle on the treeview
    def on_install_toggle(self, renderer, path):
        #print "on_install_toggle: %s %s" % (renderer, path)
        (type, name, item) = self.treeview_packages.get_model()[path]
        #print "on_install_toggle(): %s %s %s" % (type, name, item)
        # first check if the operation is save
        pkg = item.pkgname
        if not self.cache.has_key(pkg):
            return
        if self.cache[pkg].isInstalled:
            # check if it can be removed savly
            self.cache[pkg].markDelete(autoFix=False)
            if self.cache._depcache.BrokenCount > 0:
                d = gtk.MessageDialog(parent=self.window_main,
                                      flags=gtk.DIALOG_MODAL,
                                      type=gtk.MESSAGE_INFO,
                                      buttons=gtk.BUTTONS_OK)
                d.set_markup("<big><b>%s</b></big>\n\n%s" % (
                             _("Cannot remove '%s'" % pkg),
                             _("There are other applications depending "
                               "on this one. If you really want to remove "
                               "it, please use the \"Advanced\" mode from "
                               "the menu.")))
                d.run()
                d.destroy()
                self.cache[pkg].markKeep()
                assert self.cache._depcache.BrokenCount == 0
                assert self.cache._depcache.DelCount == 0
                return
            self.cache[pkg].markKeep()
            # FIXME: those assert may be a bit too strong,
            # we may just rebuild the cache if something is
            # wrong
            assert self.cache._depcache.BrokenCount == 0
            assert self.cache._depcache.DelCount == 0
        else:
            # check if it can be installed savely
            apt_error = False
            try:
                self.cache[pkg].markInstall(autoFix=True)
            except SystemError:
                apt_error = True
            if self.cache._depcache.BrokenCount > 0 or \
               self.cache._depcache.DelCount > 0 or apt_error:
                d = gtk.MessageDialog(parent=self.window_main,
                                      flags=gtk.DIALOG_MODAL,
                                      type=gtk.MESSAGE_INFO,
                                      buttons=gtk.BUTTONS_OK)
                d.set_markup("<big><b>%s</b></big>\n\n%s" % (
                             _("Cannot install '%s'" % pkg),
                             _("Installing this application would mean "
                               "that something else needs to be removed. "
                               "Please use the \"Advanced\" mode to "
                               "install '%s'." % pkg)))
                d.run()
                d.destroy()
                # reset the cache
                # FIXME: a "pkgSimulateInstall,remove"  thing would
                # be nice
                for p in self.cache.keys():
                    self.cache[p].markKeep()
                # FIXME: those assert may be a bit too strong,
                # we may just rebuild the cache if something is
                # wrong
                assert self.cache._depcache.BrokenCount == 0
                assert self.cache._depcache.DelCount == 0
                return
        # invert the current selection
        item.toInstall = not item.toInstall
        # check if the package provides multiple desktop applications
        if len(self.menu.pkg_to_app[item.pkgname]) > 1:
            apps = self.menu.pkg_to_app[item.pkgname]
            # update the install-status of the other apps
            for app in apps:
                app.toInstall = item.toInstall
            # hack: redraw the treeview (to update the toggle icons after the
            #       tree-model was changed)
            self.treeview_packages.queue_draw()
            # show something to the user (if he hasn't already seen it)
            if not item.pkgname in self.multiple_pkgs_seen:
                dia = DialogMultipleApps(self.datadir, self.window_main, \
                                         apps, item.name)
                dia.run()
                dia.hide()
                self.multiple_pkgs_seen.add(item.pkgname)

            # PORTME
#             if name in self.multiple_entry_pkgs.keys():
#                 # if the user hasn't been notified, notify
#                 if not self.multiple_entry_pkgs[name][0]:
#                     self.notifyMultiple(name)
#             if store == self.menu.store and name in self.multiple_entry_pkgs.keys():
#                 for pair in self.multiple_entry_pkgs[name][1]:
#                     store.set_value (store.get_iter(pair[1].get_path()), COL_TO_INSTALL, not store.get_value (store.get_iter(pair[1].get_path()), COL_TO_INSTALL))
#             elif store == self.search_store and name in self.search_multiples.keys():
#                 for mpath in self.search_multiples[name]:
#                     store.set_value (store.get_iter(mpath), COL_TO_INSTALL, not store.get_value (store.get_iter(mpath), COL_TO_INSTALL))
#             else:
#                 store.set_value (i, COL_TO_INSTALL, not store.get_value (i, COL_TO_INSTALL))
        self.button_apply.set_sensitive(self.menu.isChanged())

    def addChannel(self):
        """Ask for confirmation to add the missing channel or
           section of the current selected application"""
        (path, column) = self.treeview_packages.get_cursor()
        (type, name, item) = self.treeview_packages.get_model()[path]
        if item.proprietary:
            dia = DialogProprietary(self.datadir, self.window_main, item)
        else:
            dia = DialogUnavailable(self.datadir, self.window_main, item)
        res = dia.run()
        dia.hide()
        if res == gtk.RESPONSE_OK:
            if item.component:
                self.enableComponent(item.component)
            elif item.channel:
                self.enableChannel(item.channel)
        return


    def setupTreeview(self):
        def visibility_magic(cell_layout, renderer, model,iter, visible_types):
            row_type = model.get_value(iter, COL_TYPE)
            renderer.set_property("visible", row_type in visible_types)

        def package_view_func(cell_layout, renderer, model, iter):
            visibility_magic(cell_layout, renderer, model, iter,
                             (TYPE_PACKAGE,))
            row_type = model.get_value(iter, COL_TYPE)
            if row_type != TYPE_PACKAGE:
                return
            app = model.get_value(iter, COL_ITEM)
            name = app.name
            desc = app.description
            current = app.isInstalled
            future = app.toInstall
            available = app.available
            if current != future:
                markup = "<b>%s</b>\n<small>%s</small>" % (name, desc)
            else:
                markup = "%s\n<small>%s</small>" % (name, desc)
            renderer.set_property("markup", markup)

        def toggle_cell_func(column, cell, model, iter):
            row_type = model.get_value(iter, COL_TYPE)
            if row_type != TYPE_PACKAGE:
                cell.set_property("visible", False)
                return
            menuitem = model.get_value(iter, COL_ITEM)
            # Set the checkbutton sensitive, if the app can be installed
            # or removed
            if menuitem.isInstalled != False or menuitem.available == True:
                cell.set_property("sensitive", True)
            else:
                cell.set_property("sensitive", False)
            cell.set_property("active", menuitem.toInstall)
            cell.set_property("visible", True)

        def icon_cell_func(column, cell, model, iter):
            (type, menuitem) = model.get(iter, COL_TYPE, COL_ITEM)
            if menuitem == None or menuitem.icon == None:
                cell.set_property("pixbuf", None)
                cell.set_property("visible", False)
                return
            cell.set_property("pixbuf", menuitem.icon)
            cell.set_property("visible", True)

        # we have two columns, one that shows some text (header, application)
        # and one that displays if the application is going to be
        # installed/removed
        column = gtk.TreeViewColumn("")
        check_column = gtk.TreeViewColumn(_("Installed"))

        self.treeview_packages.set_search_column(COL_NAME)

        # check boxes
        self.toggle_render = gtk.CellRendererToggle()
        self.store_toggle_id = self.toggle_render.connect('toggled', self.on_install_toggle)
        #self.search_toggle_id = self.toggle_render.connect('toggled', on_install_toggle, self.search_store, self)
        #self.toggle_render.handler_block(self.search_toggle_id)
        self.toggle_render.set_property("xalign", 0.3)
        check_column.pack_start(self.toggle_render, False)
        #check_column.add_attribute(self.toggle_render, "active", COL_TO_INSTALL)
        #check_column.set_cell_data_func (self.toggle_render, visibility_magic, TYPE_PACKAGE)
        check_column.set_cell_data_func (self.toggle_render, toggle_cell_func)
        
        
        # program icons
        render = gtk.CellRendererPixbuf()
        column.pack_start(render, False)
        #column.add_attribute(render, "pixbuf", COL_ICON)
        column.set_cell_data_func (render, icon_cell_func)

        # menu group names
        render = gtk.CellRendererText()
        render.set_property("scale", 1.0)
        render.set_property("weight", 700)
        column.pack_start(render, True)
        column.add_attribute(render, "markup", COL_NAME)
        column.set_cell_data_func (render, visibility_magic, (TYPE_GROUP,))

        # package names
        render = gtk.CellRendererText()
        column.pack_start(render, True)
        column.add_attribute(render, "markup", COL_NAME)
        column.set_cell_data_func (render, package_view_func)
        
        # headers
        render = gtk.CellRendererText()
        render.set_property("xpad", 4)
        render.set_property("ypad", 4)        
        render.set_property("scale", 1.3)
        render.set_property("weight", 700)
        #self.connect("style-set", lambda parent, old, widget: widget.set_property ("foreground-gdk", parent.get_style().fg[gtk.STATE_SELECTED]), render)
        #self.connect("style-set", lambda parent, old, widget: widget.set_property ("background-gdk", parent.get_style().bg[gtk.STATE_SELECTED]), render)
        column.pack_start(render, False)
        column.add_attribute(render, "text", COL_NAME)
        column.set_cell_data_func (render, visibility_magic, (TYPE_HEADER,))

        # indent headers (i think)
        render = gtk.CellRendererText()
        render.set_property("width", self.treeview_packages.style_get_property("expander-size") + self.treeview_packages.style_get_property("horizontal-separator"))
        column.pack_start(render, False)
        column.set_cell_data_func (render, visibility_magic, (TYPE_HEADER,))

        self.treeview_packages.append_column(check_column)
        self.treeview_packages.append_column(column)

        # categories
        column = gtk.TreeViewColumn("")
        # program icons
        render = gtk.CellRendererPixbuf()
        column.pack_start(render, False)
        #column.add_attribute(render, "pixbuf", COL_ICON)
        column.set_cell_data_func (render, icon_cell_func)

        # menu group names
        self.treeview_categories.set_search_column(COL_NAME)
        render = gtk.CellRendererText()
        render.set_property("scale", 1.0)
        column.pack_start(render, True)
        column.add_attribute(render, "markup", COL_NAME)
        column.set_cell_data_func (render, visibility_magic, (TYPE_GROUP,))
        self.treeview_categories.append_column(column)
        
        
    # --------------------------------------
    # Multiple notification window functions
    # --------------------------------------
    
#     def notifyMultiple(self, pkgname):
#         if self.multiple_entry_pkgs[pkgname][0]:
#             return
#         self.multiple_store.clear()
#         for row_pair in self.multiple_entry_pkgs[pkgname][1]:
#             i = self.multiple_store.append()
#             self.multiple_store.set(i, *row_pair[0])
#         #self.multiple_view.set_model(self.multiple_store)
#         #self.multiple_win.show()
#         dia = DialogMultipleApps(self.datadir, self.window_main,
#                                  self.multiple_store)
#         dia.run()
#         dia.hide()
#         self.multiple_entry_pkgs[pkgname][0] = True
    

                
#    def populateMenu(self):
#        #def add_header(store, name):
#        #    store.set (store.append(None), COL_TYPE, TYPE_HEADER, COL_NAME, xmlescape(name), COL_ICON, None, None)
#        #
#        menu = xdg.Menu.parse(os.path.join(self.menudir, "applications.menu"))
#        # TODO: this should be done in xdg.Menu
#        #add_header(self.store, _("Applications"))
#        self.populateFromEntry(self.store, menu)
# FIXME: multiple not working
#        if self.multiple_entry_pkgs == {}:
#            for key in self.temp_dupes.keys():
#                if len(self.temp_dupes[key]) > 1:
#                    self.multiple_entry_pkgs[key] = [False, self.temp_dupes[key]]
#        self.temp_dupes.clear()
#        # mvo: disabled for now, nothing in it (ubuntu #7311)
#        #add_header(self.store, _("System Services")) # TODO. Somehow. Not sure.
#        self.treeview_packages.set_model(self.store)

    def on_browser_submit(self, browser, action, method, value):
        """Callback for the signal "submit" from the html form of the
           application description"""
        if method == "add_repo":
            self.addChannel()

    # ----------------
    # Update functions
    # ----------------
    
    def updateCache(self):
        self.window_main.set_sensitive(False)

#         # get the lock
#         try:
#             apt_pkg.PkgSystemLock()
#         except SystemError:
#             d = gtk.MessageDialog(parent=self.window_main,
#                                   flags=gtk.DIALOG_MODAL,
#                                   type=gtk.MESSAGE_ERROR,
#                                   buttons=gtk.BUTTONS_OK)
#             d.set_markup("<big><b>%s</b></big>\n\n%s" % (
#                 _("Unable to get exclusive lock"),
#                 _("This usually means that another package management "
#                   "application (like apt-get or aptitude) already running. "
#                   "Please close that application first")))
#             res = d.run()
#             d.destroy()
#             sys.exit()

        self.window_main.window.set_cursor(gtk.gdk.Cursor(gtk.gdk.WATCH))
        while gtk.events_pending():
            gtk.main_iteration()
            
        progress = GtkOpProgressWindow(self.glade,self.window_main)
        self.cache = MyCache(progress)
        #self.store.clear()
        # the new store invalidates TreeRowReferences, so make new ones.
        #self.multiple_entry_pkgs.clear()
        
        # clear search cache
        #self.filtered_cache = None

        # Setup the treeview
        self.menu = ApplicationMenu(self.desktopdir, self.cache,
                                    self.treeview_categories,
                                    self.treeview_packages,
                                    progress)
        #self.treeview_packages.set_model(self.menu.store)

        # move to "All" category per default
        # FIXME: save the the last selections state and move back to it
        self.treeview_categories.set_cursor((0,))

        #self.menu.isChanged()

        #self.updateInstalledStates()
        #self.browser.load_url("file://" + self.intro_page)
        self.browser.loadUri("file://" + self.intro_page)
        
        for file in self.info_pages.values():
            os.remove(file)
        self.info_pages.clear()
        
        adj = self.scrolled_window.get_vadjustment()
        adj.set_value(0)

        # set the toggle buttons
        val = self.config.get_bool("/apps/gnome-app-install/show_unsupported")
        if val == True or val == False:
            self.checkbutton_show_unsupported.set_active(val)
            self.menu.show_unsupported = val
        val = self.config.get_bool("/apps/gnome-app-install/show_proprietary")
        if val == True or val == False:
            self.checkbutton_show_proprietary.set_active(val)
            self.menu.show_proprietary = val
       
        self.window_main.window.set_cursor(None)
        self.window_main.set_sensitive(True)
    
    
    def ignoreChanges(self):
        """
        If any changes have been made, ask the user to apply them and return
        a value based on the status.
        Returns True if the changes should be thrown away and False otherwise
        """
        if not self.menu.isChanged():
            return True
        (to_add, to_rm) = self.menu.getChanges()
        # FIXME: move this set_markup into the dialog itself
        dia = DialogPendingChanges(self.datadir, self.window_main,
                                   to_add, to_rm)
        header =_("Apply changes to installed applications before closing?")
        msg = _("If you do not apply your changes they will be lost "\
                "permanently.")
        dia.label_pending.set_markup("<big><b>%s</b></big>\n\n%s" % \
                                     (header, msg))
        dia.button_ignore_changes.set_label(_("_Close Without Applying"))
        dia.button_ignore_changes.show()
        dia.dialog_pending_changes.realize()
        dia.dialog_pending_changes.window.set_functions(gtk.gdk.FUNC_MOVE)
        res = dia.run()
        dia.hide()
        return res

#     def updateChangeStores(self):
#         # If search results are being displayed, copy back any changes
#         if self.treeview_packages.get_model() is not self.store:
#             self.on_button_clear_clicked(None)
#         if self.new_store == None:
#             self.new_store = gtk.ListStore(gobject.TYPE_STRING, gobject.TYPE_STRING, gtk.gdk.Pixbuf.__gtype__, gobject.TYPE_STRING, gobject.TYPE_STRING, gobject.TYPE_BOOLEAN, object)
#         self.add_store.clear()
#         self.remove_store.clear()
#         self.header = _("Applications")
#         def each(store, path, i):
#             if store.get_value(i, COL_TYPE) == TYPE_HEADER:
#                 self.header = store.get_value(i, COL_NAME)
#             previous, future = store.get (i, COL_WAS_INSTALLED, COL_TO_INSTALL)
#             if previous and not future:
#                 self.remove_store.append(tuple(store[i]))
#             elif not previous and future:
#                 menu_path = [self.header]
#                 for j in range(1, len(path)):
#                     if store[ path[0:j] ][COL_NAME] != _("More programs..."):
#                         menu_path.append(store[ path[0:j] ][COL_NAME])
#                 self.new_store.append(store.get(i, COL_NAME, COL_DESC, COL_ICON, COL_PACKAGE, COL_EXEC, COL_TERMINAL) + (menu_path,))
#                 self.add_store.append(tuple(store[i]))
#         self.store.foreach(each)
        
#     def checkNewStore(self):
#         to_remove = []
#         def each(store, path, i):
#             name = store.get_value(i, COL_NAME)
#             if not self.cache[name].isInstalled:
#                 to_remove.append(gtk.TreeRowReference(store, path))
#         self.new_store.foreach(each)
#         for ref in to_remove:
#             self.new_store.remove(self.new_store.get_iter(ref.get_path()))
        
    
    # ----------------------------
    # Main window button callbacks
    # ----------------------------
    
    def on_button_help_clicked(self, widget):
        subprocess.Popen(["/usr/bin/yelp", "ghelp:gnome-app-install"])

    def on_button_advanced_clicked(self, widget):
        if self.menu.isChanged():
            ret = self.ignoreChanges()
            if ret == gtk.RESPONSE_APPLY:
                self.applyChanges(final=True)
            elif ret == gtk.RESPONSE_CANCEL:
                return False

        # Here we are either ignoring or have applied, so run Synaptic
        # TODO: activate startup notification (needs wrapping)
        os.execl("/usr/bin/gksu","gksu",
                 "--message",
                 _("Please enter your password to switch to "
                   "advanced mode"),
                 "/usr/sbin/synaptic") 
        
    def on_repositories_item_activate(self, widget):
        """ start gnome-software preferences """
        dia = SoftwareProperties(parent=self.window_main)
        dia.run()
        dia.hide()
        if dia.modified:
            # FIXME: ask about reloading here
            self.reloadSources()

    def applyChanges(self, final=False):
        #print "do_apply()"
        (to_add, to_rm) = self.menu.getChanges()
        # Set a busy cursor
        self.window_main.window.set_cursor(gtk.gdk.Cursor(gtk.gdk.WATCH))
        while gtk.events_pending():
            gtk.main_iteration()
        # Get the selections delta for the changes and apply them
        self.packageWorker.perform_action(self.window_main, to_add, to_rm)
        # Reload the APT cache and treeview
        if final != True:
            self.updateCache()

        self.button_apply.set_sensitive(self.menu.isChanged())
        
        # Show window with newly installed programs
        #self.checkNewStore() # only show things that successfully installed
        if len(to_add) > 0:
            dia = DialogNewlyInstalled(self.datadir, self.window_main,
                                       to_add, self.cache)
            dia.run()
            dia.hide()
        
        # And reset the cursor
        self.window_main.window.set_cursor(None)

    def on_button_ok_clicked(self, button):
        if self.menu.isChanged():
            ret = self.confirmChanges()
            if ret == True :
                self.applyChanges(final=True)
            else:
                return
        self.quit()

    def confirmChanges(self):
        (to_add, to_rm) = self.menu.getChanges()
        dia = DialogPendingChanges(self.datadir, self.window_main,
                                   to_add, to_rm)
        # FIXME: move this inside the dialog class, we show a different
        # text for a quit dialog and a approve dialog
        header = _("Apply the following changes?")
        msg = _("Please take a final look through the list of "\
                "applications that will be installed or removed.")
        dia.label_pending.set_markup("<big><b>%s</b></big>\n\n%s" % \
                                     (header, msg))
        res = dia.run()
        dia.hide()
        if res != gtk.RESPONSE_APPLY:
            # anything but ok makes us leave here
            return False
        else:
            return True

    def on_button_apply_clicked(self, button):
        ret = self.confirmChanges()
        if ret == True :
            self.applyChanges()

    def on_search_timeout(self):
        query = self.search_entry.get_text()
        self.menu.searchTerm = query
        self.menu._refilter()

    def on_search_entry_changed(self, widget):
        #print "on_search_entry_changed()"
        if self.search_timeout_id > 0:
            gobject.source_remove(self.search_timeout_id)
        self.search_timeout_id = gobject.timeout_add(500,self.on_search_timeout)
            
#    def on_search_entry_activate(self, widget, query=None):
#       self.window_main.set_sensitive(False)
#        self.window_main.window.set_cursor(gtk.gdk.Cursor(gtk.gdk.WATCH)) 
#        self.clearInfo()
#        while gtk.main_iteration():
#            gtk.main_iteration()
#
#        # *OMG* fix this
#        if query == None:
#            query = self.search_entry.get_text()
#        #print "Searching for \"" + query + "\""
#
#        #self.copySearchChanges()
#        #self.search_store.clear()
#        #self.on_button_clear_clicked(self.button_clear)
#
#       self.menu.search(query)
#
#        
#        self.window_main.window.set_cursor(None)
#        self.window_main.set_sensitive(True)
#        #self.button_clear.set_sensitive(True)

        
    def on_button_clear_clicked(self, button):
        self.search_entry.set_text("")
        # reset the search
        self.menu.search(None)
        # Point the treeview back to the original store
        #self.treeview_packages.set_model(self.menu.store)
        #self.treeview_packages.set_rules_hint(False)
        self.button_clear.set_sensitive(False)

    def on_item_about_activate(self, button):
        from Version import VERSION
        self.dialog_about.set_version(VERSION)
        self.dialog_about.run()
        self.dialog_about.hide()

    def on_reload_activate(self, item):
        self.reloadSources()
        
    def on_button_cancel_clicked(self, item):
        self.quit()

                
    def reloadSources(self):
        self.window_main.set_sensitive(False)

        # FIXME: reapply changes + save selected package
        
        # set the cursor to the previously selected program after reloading.
        # this only works if the path stays the same in between, which should
        # always happen.
        #path = self.treeview_packages.get_cursor()[0]
        #if self.treeview_packages.get_model() is not self.store:
        #    path = self.treeview_packages.get_model()[path][COL_PATH]
            
        #changes = self.menu.getChanges(True)
        self.packageWorker.perform_action(self.window_main,
                                          action=PackageWorker.UPDATE)
        self.updateCache()
        #self.reapplyChanges(changes)
        
        #if path != None:
        #    self.treeview_packages.expand_to_path(path)
        #    self.treeview_packages.set_cursor(path)
        

        self.window_main.set_sensitive(True)

    def enableChannel(self, channel):
        """ enables a channel with 3rd party software """
        # enabling a channel right now is very easy, just copy it in place
        channelpath = "%s/%s.list" % (self.channelsdir,channel)
        channelkey = "%s/%s.key" % (self.channelsdir,channel)
        if not os.path.exists(channelpath):
            print "WARNING: channel '%s' not found" % channelpath
            return
        #shutil.copy(channelpath,
        #            apt_pkg.Config.FindDir("Dir::Etc::sourceparts"))
        cmd = ["gksu", "--message", _("enable channel %s" % channel),
               "--",
               "cp", channelpath,
               apt_pkg.Config.FindDir("Dir::Etc::sourceparts")]
        subprocess.call(cmd)
        self.reloadSources()
                
    def enableComponent(self, component):
        """ Enables a component of the current distribution
            (in a seperate file in /etc/apt/sources.list.d/$dist-$comp)
        """
        # sanity check
        if component == "":
            print "no repo found in enableRepository"
            return

        # first find the master mirror, FIXME: something we should
        # shove into aptsources.py?
        mirror = "http://archive.ubuntu.com/ubuntu"
        sources = SourcesList()
        for source in sources:
            if source.invalid or source.disabled:
                continue
            if source.uri != mirror and sources.is_mirror(mirror,source.uri):
                mirror = source.uri
                print "found mirror: %s" % source.uri
                break
        newentry = "# automatically added by gnome-app-install on %s\n" % \
                   datetime.today()
        newentry += "deb %s %s %s\n" % (mirror, DISTRO_VERSION, component)
        channel_dir = apt_pkg.Config.FindDir("Dir::Etc::sourceparts")
        channel_file = "%s-%s.list" % (DISTRO_VERSION, component)

        channel = tempfile.NamedTemporaryFile()
        channel.write(newentry)
        channel.flush()
        print "copy: %s %s" % (channel.name, channel_dir+channel_file)
        cmd = ["gksu", "--message", _("Enable component %s" % component),
               "--",
               "install","-m","644","-o","0",
               channel.name, channel_dir+channel_file]
        print cmd
        subprocess.call(cmd)
        self.reloadSources()
            
        
    # ----------------
    # Search functions
    # ----------------
    
#     def aptSearch(self, query, resultstore):
#         # first search the apt cache
#         fcache = SimpleFilteredCache(cache=self.cache)
#         fcache.setFilter(SearchFilter(query))
#         fcache.runFilter()
#         results = fcache.keys()

#         # then search the desktop file information
#         #def match(row):
#         #    if row[COL_TYPE] == TYPE_PACKAGE and\
#         #       (row[COL_NAME] and query in row[COL_NAME].lower()) or\
#         #       (row[COL_DESC] and query in row[COL_DESC].lower()):
#         #        if not row[COL_PACKAGE] in results:
#         #            results.append(row[COL_PACKAGE])
#         #def recurse(row):
#         #    match(row)
#         #    for child in row.iterchildren():
#         #        recurse(child)
#         #for row in self.menu.store:
#         #    recurse(row)

#         # then search the store too
#         store = self.menu.store
#         for it in iterate_tree_model(store, store.get_iter_first()):
#             (type, name, item) = store[it]
#             if type == TYPE_PACKAGE:
#                 if (item.name and query in item.name.lower()) or \
#                    (item.description and query in item.description.lower()):
#                     if not item.pkgname in results:
#                         results.append(item.pkgname)
            
        # build a store of the results, adding a path column to each
        # row for later use
        #def treeSearch(row):
        #    if not row: return
        #    if self.store.get_value(row, COL_PACKAGE) in results:
        #        values = list(self.store.get(row, *range(0,14)))
        #        self.search_store.append(values + [self.store.get_path(row)])
        #    treeSearch(self.store.iter_children(row))
        #    treeSearch(self.store.iter_next(row))
        #    
        #treeSearch(self.store.get_iter_first())

        # then add the result to a search store
#         for it in iterate_tree_model(store, store.get_iter_first()):
#             (type, name, item) = store[it]
#             if type == TYPE_PACKAGE and item.pkgname in results:
#                 print "appending: %s" % name
#                 resultstore.append([type, name, item])
    
#     def mimeSearch(self, mime_type, resultstore):
        
#         def match(row):
#             matched = []
#             patterns = row[COL_MIME]
#             if patterns is not None:
#                 for repattern in patterns:
#                     # mvo: we get a list of regexp from
#                     # pyxdg.DesktopEntry.getMimeType, but it does not
#                     # use any special pattern at all, so we use the plain
#                     # pattern (e.g. text/html, audio/mp3 here)
#                     pattern = repattern.pattern
#                     if mime_type in pattern:
#                         resultstore.append(list(row))
#         def recurse(row):
#             match(row)
#             for child in row.iterchildren():
#                 recurse(child)
#         for row in self.store:
#             recurse(row)
    
    
    # ---------------------------
    # Window management functions
    # ---------------------------
    

    def on_window_main_delete_event(self, window, event):
        if window.get_property("sensitive") == False:
            return True
        if self.menu.isChanged():
            ret = self.ignoreChanges()
            if ret == gtk.RESPONSE_APPLY:
                self.applyChanges(final=True)
                self.quit()
            elif ret == gtk.RESPONSE_CANCEL:
                return True
            elif ret == gtk.RESPONSE_CLOSE:
                self.quit()
        self.quit()

    def on_window_main_destroy_event(self, data=None):
        #if self.window_installed.get_property("visible") == False:
        #    self.quit()
        self.quit()
            
    def quit(self):
        # FIXME: GARRRR, this tempfiles should be done automatically
        #        *and* they shouldn't be created on the FS at all!
        for file in self.info_pages.values():
            os.remove(file)
        os.remove(self.intro_page)
        gtk.main_quit()
            
    # -------------------
    # Info page functions
    # -------------------
    
    def generateInfoPage(self, item):
        program_name = item.name
        package_name = item.pkgname
        icon_filename = item.iconfile
        summary = item.description
        available = item.available
        section = item.component
        channel = item.channel
        desktop_environment = ""
        version = ""
        if self.cache.has_key(item.pkgname):
            version = self.cache[item.pkgname].candidateVersion
            # try to guess the used desktop environment
            for dependencies in desktop_environment_mapping:
                for dep in dependencies:
                    if self.cache.pkgDependsOn(item.pkgname, dep):
                        desktop_environment = desktop_environment_mapping[dependencies]
                        break
        data = {"name" : program_name or "",
                "summary" : summary or "",
                "bgcolor" : "white",
                "icon" : "",
                "version" : version,
                "desktop_environment" : desktop_environment
                }

        if version != "":
            data["version"] = _("Version: %s" % version)

        if icon_filename != None and icon_filename != "": 
            data["icon"] = '<img src=\"' + icon_filename + '\" align=\"right\" />'
        if available:
            pkg = self.cache[package_name]
            rough_desc = pkg.description.rstrip(" \n\t")
            
            # Replace URLs with links
            # Uncomment this when the gtkmozembed bug is fixed.
            if os.getuid() > 0:
                rough_desc = re.sub(r'(^|[\s.:;?\-\]\(<])' + 
                      r'(http://[-\w;/?:@&=+$.!~*\'()%,#]+[\w/])' +
                      r'(?=$|[\s.:;?\-\[\]>\)])',
                      r'\1<a href="\2">\2</a>', rough_desc)
                    
            # so some regular expression magic on the description
            #print "\n\nAdd a newline before each bullet:\n"
            p = re.compile(r'^(\s|\t)*(\*|0|-)',re.MULTILINE)
            rough_desc = p.sub('\n*', rough_desc)
            #print rough_desc

            #print "\n\nreplace all newlines by spaces\n"
            p = re.compile(r'\n', re.MULTILINE)
            rough_desc = p.sub(" ", rough_desc)
            #print rough_desc

            #print "\n\nreplace all multiple spaces by newlines:\n"
            p = re.compile(r'\s\s+', re.MULTILINE)
            rough_desc = p.sub("\n", rough_desc)

            lines = rough_desc.split('\n')
            #print "\n\nrough: \n"
            #print rough_desc

            clean_desc = ""
            for i in range(len(lines)):
                if lines[i].split() == []:
                    continue
                first_chunk = lines[i].split()[0]
                if first_chunk == "*":
                    p = re.compile(r'\*\s*', re.MULTILINE)
                    lines[i] = p.sub("", lines[i])
                    clean_desc += "<div class=\"item\">"\
                                  + lines[i] +\
                                  "</div>"
                else:
                    clean_desc += "<div class=\"block\">"\
                                  + lines[i] +\
                                  "</div>"
            #print "\n\nclean: \n"
            #print clean_desc
            data["description"] = clean_desc
        else:
            # FIXME: strings + code sucks
            # If the app is not available show an information 
            # and a button to add the missing repository
            msg = _("\"%s\" is not installable" % program_name)
            # check if we have seen the component
            for it in self.cache._cache.FileList:
                # FIXME: we need to exclude cdroms here. the problem is
                # how to detect if a PkgFileIterator is pointing to a cdrom
                if it.Component != "" and it.Component == item.component:
                    # warn that this app is not available on this plattform
                    msg = "<div class='block'>\n"
                    msg += _("%s is not available in any software channel." %\
                             program_name)
                    msg += "</div><div class='block'>"
                    msg += _("In the majority of cases your hardware platform"\
                             " is not supported by the application.")
                    msg += "</div>"
            if section != "":
                msg = "<div class=\"block\">"
                msg += _("The section \"%s\" of the Ubuntu software channel "\
                         "that includes \"%s\" is "\
                         "not enabled." % (section, program_name))
                msg += "</div><div class=\"block\">"
                msg += "<form name='form_add_repo' method='add_repo'"\
                       "      action='%s'>\n" % section
                msg += "     <input type='submit' name='button_add'"\
                       "            value='%s'" % _("Add Section %s" % section)
                msg += "</form>"
                msg += "</div>"
            elif channel != "":
                msg = "<div>"
                msg = _("The software channel \"%s\" that includes \"%s\" is "\
                        "not enabled." % (channel, program_name))
                msg += "</div><div class=\"block\">"
                msg += "<form name='form_add_repo' method='add_repo'"\
                       "      action='%s'>\n" % channel
                msg += "     <input type='submit' name='button_add'"\
                       "            value='%s'" % _("Add Channel %s" % channel)
                msg += "</form>"
                msg += "</div>"
            data['description'] = "<hr>%s" % msg

        
        #if package_name in self.multiple_entry_pkgs.keys():
        #    other_names = []
        #    for pair in self.multiple_entry_pkgs[package_name][1]:
        #        other_names.append(self.store.get_value(self.store.get_iter(pair[1].get_path()), COL_NAME))
        #    other_names.remove(program_name)
        #    data["multiple_info"] = _("The package that provides this program also provides the following programs: ") + ", ".join(other_names)
        #else:
        #    data["multiple_info"] = ""
        s = ""
        if self.menu.pkg_to_app.has_key(package_name) and \
               len(self.menu.pkg_to_app[package_name]) > 1:
            s = _("The package that provides this program also provides "
                  "the following programs: ")
            apps = self.menu.pkg_to_app[package_name]
            s += ", ".join([pkg.name for pkg in apps])
            data["multiple_info"] = "<hr>%s" % s
        else:
            data["multiple_info"] = ""
        
        template_file = open(os.path.join(self.datadir, "template.html"))
        template = template_file.read()
        template_file.close()
        for key in data.keys():
            template = template.replace('$' + key, data[key])
        
        filename = tempfile.NamedTemporaryFile().name
        info_page = open(filename, 'w')
        info_page.write(template)
        info_page.close()
        self.info_pages[program_name] = filename
        
        return filename
    
    def clearInfo(self):
        self.browser.loadUri("about:blank")

    def on_treeview_packages_row_activated(self, treeview, path, view_column):
        iter = treeview.get_model().get_iter(path)
        type = treeview.get_model().get_value(iter, COL_TYPE)
        if type == TYPE_GROUP:
            if treeview.row_expanded(path):
                treeview.collapse_row(path)
            else:
                treeview.expand_row(path, False)
        elif type == TYPE_PACKAGE:
            # FIXME: *garrr* ?!?
            # emitting the toggled signal with a tuple path doesn't work
            # this works around that bug
            path_str = str(path)[1:-1]
            parts = path_str.split(",")
            stripped = []
            for part in parts:
                stripped.append(part.strip())
            path_str = ":".join(stripped)
            self.toggle_render.emit("toggled", path_str)

    def on_treeview_categories_cursor_changed(self, treeview):
        path = treeview.get_cursor()[0]
        iter = treeview.get_model().get_iter(path)
        (type, name, item) = treeview.get_model()[iter]
        self.treeview_packages.set_model(item.applications)
        self.menu._refilter()
            
    def on_treeview_packages_cursor_changed(self, treeview):
        path = treeview.get_cursor()[0]
        iter = treeview.get_model().get_iter(path)
        
        if treeview.get_model().get_value(iter, COL_TYPE) != TYPE_PACKAGE:
            filename = self.intro_page
        else:
            (type, name, item) = treeview.get_model()[iter]
            #program_name, package_name, icon_filename, summary, available, section, channel = treeview.get_model().get(iter, COL_NAME, COL_PACKAGE, COL_ICON_FILE, COL_DESC, COL_AVAILABLE, COL_SECTION, COL_CHANNEL)
            if not self.info_pages.has_key(item.name):
                filename = self.generateInfoPage(item)
            else:
                filename = self.info_pages[item.name]
        
        #self.isInfoPage = True
        self.browser.loadUri("file://" + filename)
        #self.isInfoPage = False
        
    def generateIntro(self):
        text = """
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <style type="text/css">
    body {
        background: white;
        font-size: 10pt;
        font-family: Sans;
    }
    </style>
</head>
<body>"""
        # FIXME: portme
        #icon_filename = self.icon("gnome-settings-default-applications", 24)[1]
        #text += '<img src=\"' + icon_filename + '\" align=\"right\" />\n'
        text += '<h2>' + _("Quick Introduction") + '</h2>\n'
        #text += '<p>' + _("""This program allows you to add or remove programs with a
#click of a button. Browse through the categories to find programs, or search
#through all of the programs using the search box. Check the box next to a program to
#install it, and uncheck the box to remove it.""") + '</p>\n'
        text += '<p>' + _("To install an application check the box next to the "
                          "application. Accordingly uncheck the box to remove "
                          "the application.") + '</p>'
#        text += '<p>' + _("""Only simple additions and removals can be performed with
#this program. For more complicated needs, use the Synaptic Package Manager,
#which can be run by clicking "Advanced" in the "File" menu.""") + '</p>\n'
        text += '<p>' + _("To perform advanced and more complex tasks use the "
                          "package manager \"Synaptic\". Click on the button "
                          "\"Advanced\" to launch Synaptic.") + '</p>'
        text += "</body></html>"
        
        filename = tempfile.NamedTemporaryFile().name
        file = open(filename, 'w')
        file.write(text)
        file.close()
        
        return filename
        

        
      

# Entry point for testing in source tree
if __name__ == '__main__':
    app = AppInstall(os.path.abspath("menu-data"),
                     os.path.abspath("data"),
                     sys.argv)
    gtk.main()
