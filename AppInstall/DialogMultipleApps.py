# (c) 2005 Canonical, GPL

from SimpleGladeApp import SimpleGladeApp
import gtk
import gobject
import os
from Util import *
from gettext import gettext as _

class DialogMultipleApps(SimpleGladeApp):

    def __init__(self, datadir, parent, multiple_items_list, name):
        SimpleGladeApp.__init__(self,
                                path=datadir+"/gnome-app-install.glade",
                                root="dialog_multiple_apps",
                                domain="gnome-app-install")
                                
                                
        self.store = gtk.ListStore(gobject.TYPE_STRING,
                                   gobject.TYPE_PYOBJECT)
        for elm in multiple_items_list:
            self.store.append([elm.name,elm])
        self.treeview_multiple_apps.set_model(self.store)
        self.dialog_multiple_apps.set_transient_for(parent)
        self.init_treeview(self.treeview_multiple_apps)

        header = _("'%s' shares the same installation package with other "\
                   "applications" % name)
        msg = _("Modifications of one application affect the other "\
                "applications, too.")
        self.label_multiple.set_markup("<b><big>%s</big></b>\n\n%s" %\
                                       (header, msg))

    def run(self):
        return self.dialog_multiple_apps.run()

    def hide(self):
        self.dialog_multiple_apps.hide()

    # this code is identical to DialogPendingChanges - merge
    def init_treeview(self, view):
        def package_view_func(cell_layout, renderer, model, iter):
            (name, item) = model.get(iter, 0, 1)
            renderer.set_property("markup",
                                  "%s\n<small>%s</small>" % \
                                  (item.name, item.description))
        def icon_cell_func(column, cell, model, iter):
            menuitem = model.get_value(iter, 1)
            if menuitem.icon != None:
                cell.set_property("pixbuf", menuitem.icon)
            else:
                cell.set_property("pixbuf", None)
            cell.set_property("visible", True)
        view.set_search_column(COL_NAME)
        view.get_selection().set_mode(gtk.SELECTION_NONE)
        column = gtk.TreeViewColumn("")
        render = gtk.CellRendererPixbuf()
        column.pack_start(render, False)
        column.set_cell_data_func (render, icon_cell_func)
        render = gtk.CellRendererText()
        render.set_property("xpad", 4)
        column.pack_start(render, True)
        column.add_attribute(render, "markup", COL_NAME)
        column.set_cell_data_func (render, package_view_func)
        view.append_column(column)
