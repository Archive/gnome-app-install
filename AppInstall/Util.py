# misc utils
# (c) 2005 Canonical, GPL

import warnings
warnings.filterwarnings("ignore", "apt API not stable yet", FutureWarning)
import apt


# Column enumeration
#(COL_TYPE,
# COL_WAS_INSTALLED,
# COL_TO_INSTALL,
# COL_NAME,
# COL_DESC,
# COL_ICON,
# COL_ICON_FILE,
# COL_PACKAGE,
# COL_MIME,
# COL_EXEC,
# COL_TERMINAL,
# COL_AVAILABLE,
# COL_SECTION,
# COL_CHANNEL,
# COL_PATH) = range(0, 15)


# Columns
(COL_TYPE,
 COL_NAME,
COL_ITEM) = range(0,3)

# Row types
(TYPE_HEADER,
 TYPE_GROUP,
 TYPE_PACKAGE) = range(0, 3)

def xmlescape(s):
    from xml.sax.saxutils import escape
    if s==None:
        return ""
    else:
        return escape(s)


def iterate_tree_model(store, it):
    """ iterate over a gtk tree-model, returns a gtk.TreeIter for each element
    """
    if it:
        # down
        for elm in iterate_tree_model(store, store.iter_children(it)):
            yield elm
        # right
        for elm in iterate_tree_model(store, store.iter_next(it)):
            yield elm
        # current elem
        yield it



# class SimpleFilteredCache(apt.cache.FilteredCache):
#     """ a simpler version of the filtered cache that will not react to
#         cache changed (no need, we are only interessted in text)
#     """
#     def filterCachePostChange(self):
#         pass
#     def runFilter(self):
#         self._reapplyFilter()

# class SearchFilter(apt.cache.Filter):
#     """ a filter class that just searchs insensitive in name/description """
#     def SetSearchTerm(self, term):
#         self._term = term.lower()
#     def apply(self, pkg):
#         if self._term in pkg.name.lower() or \
#                self._term in pkg.description.lower():
#             return True
#         else:
#             return False
#     def __init__(self, query=None):
#         if query != None:
#             self.SetSearchTerm(query)



