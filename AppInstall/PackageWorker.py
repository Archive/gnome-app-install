# (c) 2005 Canonical, GPL

import apt_pkg
import subprocess
import gtk
import thread
import time
import os
import tempfile
from gettext import gettext as _

class PackageWorker:
    """
    A class which does the actual package installing/removing.
    """

    # synaptic actions
    (INSTALL, UPDATE) = range(2)
    
    def run_synaptic(self, id, lock, to_add=None,to_rm=None, action=INSTALL):
        #apt_pkg.PkgSystemUnLock()
        #print "run_synaptic(%s,%s,%s)" % (id, lock, selections)
        msg = "<b><big>"
        msg += _("Administration rights are required to install and remove "\
                 "applications")
        msg += "</big></b>\n\n"
        msg += _("Enter your password to grant administration rights.")
        cmd = ["/usr/bin/gksu",
               "--message", msg,
               "--",
               "/usr/sbin/synaptic",
               "--hide-main-window",
               "--non-interactive",
               "--parent-window-id", "%s" % (id) ]

        # create tempfile for install (here because it must survive
        # durng the synaptic call
        f = tempfile.NamedTemporaryFile()
        if action == self.INSTALL:
            for item in to_add:
                f.write("%s\tinstall\n" % item.pkgname)
                print item.pkgname
            for item in to_rm:
                f.write("%s\tuninstall\n" % item.pkgname)
            cmd.append("--set-selections-file")
            cmd.append("%s" % f.name)
            f.flush()
        elif action == self.UPDATE:
            #print "Updating..."
            cmd.append("--update-at-startup")
        subprocess.call(cmd)
        lock.release()
        f.close()

    def plug_removed(self, w, (win,socket)):
        # plug was removed, but we don't want to get it removed, only hiden
        # unti we get more
        win.hide()
        return True

    def plug_added(self, sock, win):
        while gtk.events_pending():
            win.set_position(gtk.WIN_POS_CENTER_ON_PARENT)
            win.show()
            print "huhu"
            gtk.main_iteration()

    def get_plugged_win(self, window_main):
        win = gtk.Window()
        win.realize()
        win.window.set_functions(gtk.gdk.FUNC_MOVE)
        win.set_border_width(6)
        win.set_transient_for(window_main)
        win.set_position(gtk.WIN_POS_CENTER_ON_PARENT)
        win.set_title("")
        win.set_resizable(False)
        win.set_property("skip-taskbar-hint", True)
        win.set_property("skip-taskbar-hint", True)
        # prevent the window from closing with the delete button (there is
        # a cancel button in the window)
        win.connect("delete_event", lambda e,w: True);
    
        # create the socket
        socket = gtk.Socket()
        socket.show()
        win.add(socket)
        
        socket.connect("plug-added", self.plug_added, win)
        socket.connect("plug-removed", self.plug_removed, (win,socket))

        
        return win, socket
    
    def perform_action(self, window_main, to_add=None, to_rm=None, action=INSTALL):
        window_main.set_sensitive(False)
        #plug_win, socket = self.get_plugged_win(window_main)
        
        lock = thread.allocate_lock()
        lock.acquire()
        t = thread.start_new_thread(self.run_synaptic,(window_main.window.xid,lock,to_add, to_rm, action))
        while lock.locked():
            while gtk.events_pending():
                gtk.main_iteration()
            time.sleep(0.05)
        #plug_win.destroy()
        window_main.set_sensitive(True)
