# (c) 2005 Canonical, GPL

from SimpleGladeApp import SimpleGladeApp
import gtk
import gobject
import os
from gettext import gettext as _
from BrowserView import GtkHtml2BrowserView as BrowserView

class DialogProprietary(SimpleGladeApp):

    def __init__(self, datadir, parent, item):
        SimpleGladeApp.__init__(self,
                                path=datadir+"/gnome-app-install.glade",
                                root="dialog_proprietary",
                                domain="gnome-app-install")
        msg = "<b><big>%s</big></b>" %  _("Add the required third party "\
                                          "channel \"%s\"?" % item.channel)
        msg += "\n\n"
        msg += _("This application is distributed in a third "
                "party software channel, only. To install "
                "you have to add the corresponding channel.\n"\
                "You need a working internet connection to continue")
        self.dialog_proprietary.set_transient_for(parent)
        self.dialog_proprietary.realize()
        self.dialog_proprietary.window.set_functions(gtk.gdk.FUNC_MOVE)
        self.label_proprietary.set_markup(msg)
        self.item = item

    def timeout(self):
            self.browser.loadUri(self.item.licenseUri)
            return False
    def run(self):
        if self.item.licenseUri:
            msg = self.label_proprietary.get_label()
            msg += "\n\n"
            msg += _("The application comes with the following license "
                     "terms and conditions. If you click on the button 'Add' "
                     "you accept them:")
            self.label_proprietary.set_markup(msg)
            self.tooltips = gtk.Tooltips()
            self.tooltips.set_tip(self.button_add_channel, \
                                  _("Accept the license terms and add the "\
                                    "software channel"))
            self.button_add_channel.grab_default()
            self.browser.show()
            gobject.timeout_add(1, self.timeout)
        return self.dialog_proprietary.run()

    def hide(self):
        self.dialog_proprietary.hide()

    def create_proprietary_browser_view(self, s1, s2, i1, i2):
        #print "create_custom_browser_view()"
        self.browser = BrowserView()
        return self.browser

