# (c) 2005 Canonical, GPL

import pygtk; pygtk.require("2.0")
import gtk
import gtk.gdk
import gobject
import xdg.Menu
import os
import gettext

from warnings import warn
from gettext import gettext as _

from Util import *

class MenuItem(object):
    " base class for a object in the menu "
    def __init__(self, name, icon=None, iconfile=None):
        # the name that is displayed
        self.name = name
        # the icon that is displayed
        self.icon = icon
        self.iconfile = iconfile
    def __repr__(self):
        return "MenuItem: %s" % self.name

class Category(MenuItem):
    """ represents a category """
    def __init__(self, parent, name, icon=None, iconfile=None):
        MenuItem.__init__(self, name, icon, iconfile)
        # if that category has applications, add them to the
        # store here
        self.real_applications_store = gtk.ListStore(gobject.TYPE_INT,
                                                     gobject.TYPE_STRING,
                                                     gobject.TYPE_PYOBJECT)
        self.applications = self.real_applications_store.filter_new()
        self.applications.set_visible_func(parent._visible_filter)


class Application(MenuItem):
    """ this class represents a application """
    def __init__(self, name, icon=None, iconfile=None):
        MenuItem.__init__(self, name, icon, iconfile)
        # the apt-pkg name that belongs to the app
        self.pkgname = None
        # the description
        self.description = ""
        # a html page
        self.html = None
        # mime type
        self.mime = None
        # exec
        self.execCmd = None
        # needsTerminal
        self.needsTerminal = False
        # we have it right now
        self.available = False
        # component the package is in (main, universe, multiverse, restricted)
        self.component = None
        # channel the pacakge is in (e.g. skype)
        self.channel = None
        # states
        self.isInstalled = False
        self.toInstall = False
        # proprietary
        self.proprietary = False
        self.licenseUri = None
        self.supported = False
        # textual menu path
        self.menupath = ""

class ApplicationMenu(object):
    """ this represents the application menu, the interessting bits are:
        - store that can be attached to a TreeView
        - pkg_to_app a dictionary that maps the apt pkgname to the application
                     items
    """

    def __init__(self, datadir, cache, treeview_categories,
                 treeview_packages, progress):
        self.menudir = datadir+"/desktop"
        self.cache = cache
        self.treeview_categories = treeview_categories
        self.treeview_packages = treeview_packages

        # get the featured applications
        #featured_file = open(os.path.join(datadir, "featured.txt"))
        #self.featured_list = featured_file.read().split()

        # icon theme
        self.icons = gtk.icon_theme_get_default()
        self.icons.prepend_search_path(os.path.join(datadir, "icons"))
        gtk.window_set_default_icon(self.icons.load_icon("gnome-settings-default-applications", 32, 0))

        # search
        self.searchTerm = None
        
        # properties for the view
        self.__show_proprietary = False
        self.__show_unsupported = True

        # a dictonary that provides a mapping from a pkg to the
        # application names it provides
        self.pkg_to_app = {}

        self.real_categories_store = gtk.ListStore(gobject.TYPE_INT,
                                                 gobject.TYPE_STRING,
                                                 gobject.TYPE_PYOBJECT)

        # populate the tree
        self.refresh(progress)
        self.store = self.real_categories_store

      
        #self.store = self.real_category_store.filter_new()
        #self.store.set_visible_func(self._visible_filter)

        # setup search store + filter
        #self.real_search_store = gtk.ListStore(gobject.TYPE_INT,
        #                                       gobject.TYPE_STRING,
        #                                       gobject.TYPE_PYOBJECT)
        #self.search_store = self.real_search_store.filter_new()
        #self.search_store.set_visible_func(self._visible_filter)

        # default is the tree
        self.treeview_categories.set_model(self.real_categories_store)
        self.treeview_packages.set_model(None)

    # properties
    
    # supported 
    def get_show_unsupported(self):
        return self.__show_unsupported
    def set_show_unsupported(self, new_value):
        self.__show_unsupported = new_value
        self._refilter()
    show_unsupported = property(get_show_unsupported, set_show_unsupported)

    # proprietary
    def get_show_proprietary(self):
        return self.__show_proprietary
    def set_show_proprietary(self, new_value):
        self.__show_proprietary = new_value
        self._refilter()
    show_proprietary = property(get_show_proprietary, set_show_proprietary)


    # helpers
    def _refilter(self):
        # we need to disconnect the model from the view when we
        # do a refilter, otherwise we get random crashes in the search
        # (to reproduce:
        #  1. open "accessability" 2. unselect "show unsupported"
        #  3. search for "apt" 4. turn "show unsupported" on/off -> BOOM
        model = self.treeview_packages.get_model()

        # save the cursor position (or rather, the name of the app selected)
        name = None
        (path, colum) = self.treeview_packages.get_cursor()
        if path:
            if model.get_value(model.get_iter(path), COL_TYPE) == TYPE_PACKAGE:
                name = model.get_value(model.get_iter(path), COL_NAME)
                #print "found: %s (%s) " % (name, path)

        # this is the actual refiltering
        self.treeview_packages.set_model(None)
        if model != None:
            model.refilter()
        self.treeview_packages.set_model(model)

        # redo the cursor
        if name != None:
            for it in iterate_tree_model(model, model.get_iter_first()):
                aname = model.get_value(it, COL_NAME)
                if name == aname:
                    #print "selecting: %s (%s)" % (name, model.get_path(it))
                    #self.treeview_packages.expand_to_path(model.get_path(it))
                    self.treeview_packages.set_cursor(model.get_path(it))
                    return
    
    def _visible_filter(self, model, iter):
        (type, item) = model.get(iter, COL_TYPE, COL_ITEM)
        if type == TYPE_PACKAGE and item:
            if item.proprietary and not self.show_proprietary:
                #print "not showing: %s (%s)" % (item.name, model)
                return False
            if item.supported == False and not self.show_unsupported:
                #print "not showing: %s (unsupported)" % item.name
                return False
            if self.searchTerm:
                if self.searchTerm in item.name.lower() or \
                   self.searchTerm in item.pkgname.lower() or \
                   (item.description and
                     self.searchTerm in item.description.lower()) or \
                   (self.cache.has_key(item.pkgname) and
                     self.searchTerm in self.cache[item.pkgname].description.lower()):
                    return True
                return False
        return True

    # user visible stuff
    
    def search(self, query):
        # this is a "reset"
        if query == None:
            self.treeview_packages.set_model(self.store)
            self._refilter()
            self.treeview_packages.set_rules_hint(False)
            return
        # a real search, disconnect the view first, otherwise it segfautls
        self.treeview_packages.set_model(None)
        self.real_search_store.clear()
        query = query.lower()
        if query.startswith("mime-type:"):
            mime_query = query[len("mime-type:"):].strip()
            #self.mimeSearch(mime_query,store)
        #else:
        #    self.aptSearch(query,store)

        # do the search
        for it in iterate_tree_model(self.real_store,
                                      self.real_store.get_iter_first()):
            (type, name, item) = self.real_store[it]
            if item == None or type != TYPE_PACKAGE:
                continue
            #print "looking at: %s" % item.name
            if (item.name and query in item.name.lower()) or \
               (item.description and query in item.description.lower()) or \
               (item.pkgname and query in item.pkgname) or \
               (item.pkgname and self.cache.has_key(item.pkgname) and query in self.cache[item.pkgname].description.lower()):
                self.real_search_store.append([type, name, item])
        
        # If no results are found, add a placeholder
        if len(self.real_search_store) == 0:
            self.real_search_store.set(self.real_search_store.append(None),
                             COL_TYPE, TYPE_HEADER,
                             COL_NAME, xmlescape(_("No results found")))
        # Show the search results in the tree view
        self.treeview_packages.set_model(self.search_store)
        self.treeview_packages.set_rules_hint(True)


    def refresh(self, progress):        
        menu = xdg.Menu.parse(os.path.join(self.menudir, "applications.menu"))
        progress.allItems = len([x for x in menu.getEntries()])
        progress.subOp = _("Reading desktop files")
        progress.progressbar_cache.set_pulse_step(0.07)
        self.real_categories_store.clear()

        # add "All" category
        self.all_category_iter = self.real_categories_store.append()
        icon, icon_file = self._getIcon("distributor-logo", 32)
        item = Category(self, "<b>%s</b>" % _("All"), icon, icon_file)
        item.real_applications_store.set_sort_column_id(COL_NAME,
                                                        gtk.SORT_ASCENDING)
        self.real_categories_store.set(self.all_category_iter,
                                       COL_TYPE, TYPE_GROUP,
                                       COL_NAME, "<b>%s</b>" % _("All"),
                                       COL_ITEM, item)

        # populate the rest
        self._populateFromEntry(menu, progress=progress)
        #self._updateInstalledStates(self.store)

    def getChanges(self, get_paths=False):
        """ return the selected changes in the tree
            TODO: what is get_paths?
        """
        to_inst = set()
        to_rm = set()
        for (type, name, item) in self.store:
            for (type,name,item) in item.real_applications_store:
                if type != TYPE_PACKAGE:
                    continue
                if item.isInstalled and not item.toInstall:
                    to_rm.add(item)
                if not item.isInstalled and item.toInstall:
                    to_inst.add(item)
        #print "to_add: %s" % to_inst
        #print "to_rm: %s" % to_rm
        return (to_inst, to_rm)
        
    def isChanged(self):
        """ check if there are changes at all """
        for (type, cat_name, cat)  in self.store:
            for (type,name,item) in cat.real_applications_store:
                if type == TYPE_PACKAGE and item.toInstall != item.isInstalled:
                    return True
        return False

     # FIXME: PORTME
#    def reapplyChanges(self, to_add, to_rm):
#         for item in selections:
#             # if the updated store and the old store don't have the same paths, this will break.
#             # that shouldn't happen.
#             pkgname, available = self.store.get(self.store.get_iter(item[1]), COL_PACKAGE, COL_AVAILABLE)
#             # don't copy back changes to an uninstallable package
#             if not available:
#                 continue
#             if not pkgname in item[0]:
#                 print "ERROR: Package name mismatch while reapplying changes"
#             if "uninstall" in item[0]:
#                 to_install = False
#             elif "install" in item[0]:
#                 to_install = True
#             else: # this should never happen.
#                 continue
#             self.store.set(self.store.get_iter(item[1]), COL_TO_INSTALL, to_install)

    # helpers

    def _populateFromEntry(self, node, parent = None, progress=None):
        #print "%s/%s" % (len(store), progress.allItems)
        progress.update((len(self.real_categories_store)/float(progress.allItems))*100)
        # for some reason xdg hiddes some entries, but we don't like that
        for entry in node.getEntries(hidden=True):
            #print "entry: %s " % (entry)
            if isinstance(entry, xdg.Menu.Menu):
                # we found a toplevel menu
                icon, icon_file = self._getIcon(entry.getIcon(), 32)
                name = xmlescape(entry.getName())
                item = Category(self, name, icon, icon_file)
                #print "adding: %s" % name
                self.real_categories_store.set(self.real_categories_store.append(),
                                               COL_TYPE, TYPE_GROUP,
                                               COL_NAME, name,
                                               COL_ITEM, item)
                self._populateFromEntry(entry, item,  progress=progress)
            elif isinstance(entry, xdg.Menu.MenuEntry):
                # we found a application
                name = xmlescape(entry.DesktopEntry.getName())
                if not name:
                    print "no name found for: %s" % entry
                if name and entry.DesktopEntry.hasKey("X-AppInstall-Package"):
                    store = parent.real_applications_store
                    item = Application(name)
                    pkgname = entry.DesktopEntry.get("X-AppInstall-Package")
                    item.pkgname = pkgname
                    # figure component and support status
                    item.component = entry.DesktopEntry.get("X-AppInstall-Section")
                    supported =  entry.DesktopEntry.get("X-AppInstall-Supported")
                    if supported != "":
                        item.supported = bool(supported)
                    else:
                        if item.component == "main" or \
                               item.component == "restricted":
                            item.supported = True
                            
                    # check the proprietary flag
                    item.channel = entry.DesktopEntry.get("X-AppInstall-Channel")
                    proprietary =  entry.DesktopEntry.get("X-AppInstall-Proprietary")
                    if proprietary != "":
                        item.proprietary = bool(proprietary)
                        item.licenseUri = entry.DesktopEntry.get("X-AppInstall-LicenseUri")
                    if self.cache.has_key(item.pkgname):
                        item.available = True
                    else:
                        item.available = False
                    icon, icon_file = self._getIcon(entry.DesktopEntry.get("X-AppInstall-Icon", "") or entry.DesktopEntry.getIcon(), 24)
                    item.icon = icon
                    item.iconfile = icon_file
                    if self.cache.has_key(pkgname):
                        item.isInstalled = self.cache[pkgname].isInstalled
                    else:
                        item.isInstalled = False
                    item.toInstall = item.isInstalled
                    item.description = xmlescape(entry.DesktopEntry.getComment())
                    item.mime = entry.DesktopEntry.getMimeType()
                    item.execCmd = entry.DesktopEntry.getExec()
                    item.needsTerminal = entry.DesktopEntry.getTerminal()
                    item.menupath = [_("Applications"),parent.name]
                    #print item.menupath
                    store.set(store.append(),
                              COL_TYPE, TYPE_PACKAGE,
                              COL_NAME, name,
                              COL_ITEM, item)

                    # add to "All"
                    store = self.real_categories_store.get_value(self.all_category_iter, COL_ITEM).real_applications_store
                    store.set(store.append(),
                              COL_TYPE, TYPE_PACKAGE,
                              COL_NAME, name,
                              COL_ITEM, item)
                    
                    # populate the dictionary from pkgname to provided apps as
                    # well
                    if self.pkg_to_app.has_key(pkgname):
                        if not name in [pkg.name for pkg in self.pkg_to_app[pkgname]]:
                            self.pkg_to_app[pkgname].append(item)
                    else:
                        self.pkg_to_app[pkgname] = [item]
                else:
                    try:
                        print "Got non-package menu entry %s" % entry
                    except UnicodeEncodeError:
                        pass
            elif isinstance(entry, xdg.Menu.Header):
                print "got header"

    def _getIcon(self, name, size):
        if name is None or name == "":
            warn("ICON: Using dummy icon")
            name = "gnome-other"
        if name.startswith("/"):
            warn("ICON: Doesn't handle absolute paths: '%s'" % name)
            name = "gnome-other"
        if name.find(".") != -1:
            import os.path
            # splitting off extensions in all cases broke evolution's icon
            # hopefully no common image extensions will ever have len != 3
            if len(os.path.splitext(name)[1]) == (3 + 1): # extension includes '.'
                name = os.path.splitext(name)[0]
        if not self.icons.has_icon(name):
            warn("ICON: Icon '%s' is not in theme" % name)
            name = "gnome-other"
        # FIXME: mvo: this try: except is a hack to work around 
        #             ubuntu #6858 (icon is no longer in cache after removal)
        #             of a pkg. correct is probably to reload the icontheme
        #             (or something)
        try:
            icon = self.icons.load_icon(name, size, 0)
        except gobject.GError:
            icon = self.icons.load_icon("gnome-other", size, 0)
            name = "gnome-other"
        if icon.get_width() != size:
            warn("ICON: Got badly sized icon for %s" % name)
            icon = icon.scale_simple(size, size, gtk.gdk.INTERP_BILINEAR)
        
        info = self.icons.lookup_icon(name, size, gtk.ICON_LOOKUP_NO_SVG | gtk.ICON_LOOKUP_USE_BUILTIN)
        if info is None:
            info = self.icons.lookup_icon("gnome-other", size, gtk.ICON_LOOKUP_NO_SVG | gtk.ICON_LOOKUP_USE_BUILTIN)
        filename = info.get_filename()
        return icon, filename

