# (c) 2005 Canonical, GPL

from SimpleGladeApp import SimpleGladeApp
import gtk
import gobject
import os

from Util import *

class DialogPendingChanges(SimpleGladeApp):
    def __init__(self, datadir, parent, to_add, to_rm):
        SimpleGladeApp.__init__(self,
                                path=datadir+"/gnome-app-install.glade",
                                root="dialog_pending_changes",
                                domain="gnome-app-install")
        self.add_store = gtk.ListStore(gobject.TYPE_STRING,
                                       gobject.TYPE_PYOBJECT)
        self.remove_store = gtk.ListStore(gobject.TYPE_STRING,
                                          gobject.TYPE_PYOBJECT)
        for elm in to_add:
            self.add_store.append([elm.name,elm])
        for elm in to_rm:
            self.remove_store.append([elm.name,elm])
        self.treeview_pending_add.set_model(self.add_store)
        self.treeview_pending_remove.set_model(self.remove_store)
        self.dialog_pending_changes.set_transient_for(parent)
        self.init_treeview(self.treeview_pending_add)
        self.init_treeview(self.treeview_pending_remove)
        self.button_confirm_changes.grab_default()
        self.dialog_pending_changes.realize()
        self.dialog_pending_changes.window.set_functions(gtk.gdk.FUNC_MOVE)

    # this code is identical to DialogMultipleApps - merge
    def init_treeview(self, view):
        def package_view_func(cell_layout, renderer, model, iter):
            (name, item) = model.get(iter, 0, 1)
            renderer.set_property("markup",
                                  "%s\n<small>%s</small>" % \
                                  (item.name, item.description))
        def icon_cell_func(column, cell, model, iter):
            menuitem = model.get_value(iter, 1)
            if menuitem.icon != None:
                cell.set_property("pixbuf", menuitem.icon)
            else:
                cell.set_property("pixbuf", None)
            cell.set_property("visible", True)
        view.set_search_column(COL_NAME)
        view.get_selection().set_mode(gtk.SELECTION_NONE)
        column = gtk.TreeViewColumn("")
        render = gtk.CellRendererPixbuf()
        column.pack_start(render, False)
        column.set_cell_data_func (render, icon_cell_func)
        render = gtk.CellRendererText()
        render.set_property("xpad", 4)
        column.pack_start(render, True)
        column.add_attribute(render, "markup", COL_NAME)
        column.set_cell_data_func (render, package_view_func)
        view.append_column(column)

    def run(self):
        if len(self.add_store) == 0:
            self.vbox_add.hide()
        if len(self.remove_store) == 0:
            self.vbox_remove.hide()
        return self.dialog_pending_changes.run()

    def hide(self):
        self.dialog_pending_changes.hide()
        
