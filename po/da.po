# Danish translation of Gnome application installer.
# Copyright (C) 2005 Ross Burton
# This file is distributed under the same license as the gnome-app-install package.
# Martin Willemoes Hansen <mwh@sysrq.dk>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-app-install\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2005-03-12 16:49+0100\n"
"PO-Revision-Date: 2005-03-12 16:54+0100\n"
"Last-Translator: Martin Willemoes Hansen <mwh@sysrq.dk>\n"
"Language-Team: Danish <dansk@klid.dk>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Setup the window
#: ../src/AppInstall.py:82 ../data/gnome-app-install.glade.h:2
msgid "Application Installer"
msgstr "Programinstallatør"

#: ../src/AppInstall.py:228
msgid "Applications"
msgstr "Programmer"

#: ../src/AppInstall.py:230
msgid "System Services"
msgstr "Systemtjenester"

#: ../src/AppInstall.py:264
msgid ""
"<big><b>Changes Pending</b></big>\n"
"\n"
"You have selected to add or remove applications, but these changes have not "
"been applied.  Do you want to ignore them, or do them now?"
msgstr ""
"<big><b>Forestående ændringer</b></big>\n"
"\n"
"Du har valgt at tilføje eller fjerne programmer, men disse ændringer er "
"endnu ikke blevet anvendt. Vil du ignorere dem eller gøre dem nu?"

#: ../src/AppInstall.py:266
msgid "Ignore"
msgstr "Ignorér"

#: ../src/AppInstall.py:267
msgid "Apply"
msgstr "Anvend"

#: ../data/gnome-app-install.glade.h:1
msgid "<span size='xx-large'><b>Add or Remove Applications</b></span>"
msgstr "<span size='xx-large'><b>Tilføj eller fjern programmer</b></span>"

#: ../data/gnome-app-install.glade.h:3
msgid "_Advanced"
msgstr "_Avanceret"
